Source: first-last-agg
Section: database
Priority: optional
Maintainer: Debian PostgreSQL Maintainers <team+postgresql@tracker.debian.org>
Uploaders:
 Christoph Berg <myon@debian.org>,
Build-Depends:
 architecture-is-64-bit <!pkg.postgresql.32-bit>,
 debhelper-compat (= 13),
 postgresql-all <!nocheck>,
 postgresql-server-dev-all (>= 217~),
Standards-Version: 4.7.0
Rules-Requires-Root: no
Vcs-Browser: https://salsa.debian.org/postgresql/first-last-agg
Vcs-Git: https://salsa.debian.org/postgresql/first-last-agg.git
Homepage: https://github.com/wulczer/first_last_agg

Package: postgresql-17-first-last-agg
Architecture: any
Depends:
 ${misc:Depends},
 ${postgresql:Depends},
 ${shlibs:Depends},
Description: PostgreSQL extension providing first and last aggregate functions
 first-last-agg is a simple extension providing two aggregate functions, last
 and first, operating on any element type and returning the last or the first
 value of the group.
 .
 Since by default the ordering inside groups created by a GROUP BY expression
 is not defined, it is advisable to use an ORDER BY clause inside the aggregate
 expression (see the aggregate function syntax).
